from portal import app
from flask import render_template, request, session
import os
import flask
import json
import popen2
import time
import shutil

@app.route("/")
def index():
	return "SVN API"

@app.route("/<api>/svn", methods = ['GET', 'POST', 'DELETE'])
def repro(api):
	if(login(api)):
		if request.method == 'GET':
			folderList = os.listdir(app.config["svn"])
			reproList = list()
			for repro in folderList:
				print repro
				if(os.path.isdir(app.config["svn"]+"/"+repro)):
					reproList.append(repro)
			return json.dumps(reproList)
		elif request.method == 'POST':
			print app.config["svn"]+"/"+request.json["name"]
			os.mkdir(app.config["svn"]+"/"+request.json["name"])
			popen2.popen3("svnadmin create "+app.config["svn"]+"/"+request.json["name"]+"/")
			time.sleep(5)
			fobj = open(app.config["cofigFile"], "r")
			config = ""
			for line in fobj:
				config = config + line
			fobj.close()
			fobj = open(app.config["svn"]+"/"+request.json["name"]+"/conf/svnserve.conf", "w")
			fobj.write(config)
			fobj.close()

		elif request.method == 'DELETE':
			shutil.rmtree(app.config["svn"]+"/"+request.json["name"])
	else:
		return json.dumps("Wront API")
	return "RETURN"

def login(api):
	if(api in app.config["api"]):
		return True
	return False